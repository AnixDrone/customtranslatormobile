import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomePageRoutingModule } from './home-routing.module';

import { HomePage } from './pages/home/home.page';
import {LanguagePage} from './pages/language/language.page';
import {SidemenuComponent} from './pages/sidemenu/sidemenu.component';
import {WordService} from './word.service';
import {LanguageService} from './language.service';



const pages = [
    HomePage,
    LanguagePage,
    SidemenuComponent
];

const views = [];

const services = [
    WordService,
    LanguageService
];

const modules = [
  CommonModule,
  FormsModule,
  IonicModule,
  HomePageRoutingModule
];


@NgModule({
    imports: [...modules],
    declarations: [...pages, ...views],
    providers: [...services],
    exports: [
        SidemenuComponent
    ]
})
export class HomePageModule {}
