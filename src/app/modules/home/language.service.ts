import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Language} from './interface/language';
import {Constants} from '../constants/Constants';
import {LanguageRequest} from './interface/languageRequest';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  constructor(private _http:HttpClient) { }

  languages:Observable<Language[]>;

  setLanguage(){
    this.languages=this.getAllLanguages();
  }

  getAllLanguages(): Observable<Language[]> {
    return this._http.get<Language[]>(`${Constants.apiUrl}lang/`);
  }

  addNewLanguage(languageRequest: LanguageRequest) {
    return this._http.post(`${Constants.apiUrl}lang/`,languageRequest);
  }

  getLanguageWithId(id: string): Observable<Language> {
    return this._http.get<Language>(`${Constants.apiUrl}lang/${id}`);
  }
}
