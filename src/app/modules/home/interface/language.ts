export interface Language {
    id:number,
    originalLanguage:string,
    translatedLanguage:string
}
