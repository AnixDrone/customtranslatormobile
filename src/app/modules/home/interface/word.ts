export interface Word {
    original:string,
    translated:string,
    langId:string,
    id:number,
}
