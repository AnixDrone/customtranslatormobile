export interface WordRequest {
    original:string,
    translated:string,
    langId:number
}
