export interface LanguageRequest {
    originalLanguage:string,
    translatedLanguage:string
}
