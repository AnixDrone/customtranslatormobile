import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomePage } from './pages/home/home.page';
import {LanguagePage} from './pages/language/language.page';

const routes: Routes = [

  {
    path:'',
    component:HomePage
  },
  {
    path:':id',
    component:LanguagePage
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomePageRoutingModule {

}
