import {Component, OnInit} from '@angular/core';
import {LanguageService} from '../../language.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(private languageService:LanguageService) { }

  ngOnInit() {
  }

  doRefresh(event){
    this.languageService.setLanguage();
    this.languageService.getAllLanguages().subscribe(()=>{
      event.target.complete();
    })
  }
}
