import {Component, OnInit} from '@angular/core';

import {ActivatedRoute} from '@angular/router';
import {Word} from '../../interface/word';
import {Language} from '../../interface/language';
import {WordRequest} from '../../interface/wordRequest';
import {animate, style, transition, trigger} from '@angular/animations';
import {WordService} from '../../word.service';
import {LanguageService} from '../../language.service';
import {ToastController} from '@ionic/angular';


@Component({
    selector: 'app-language',
    templateUrl: './language.page.html',
    styleUrls: ['./language.page.scss'],
    animations: [
        trigger('fadeSlideInOut', [
            transition(':enter', [
                style({opacity: 0, transform: 'translateY(-30px)'}),
                animate('100ms', style({opacity: 1, transform: 'translateY(0)'})),
            ]),
            transition(':leave', [
                animate('100ms', style({opacity: 0, transform: 'translateY(-30px)'})),
            ]),
        ]),
        trigger('fadeSlideHorizontal', [
            transition(':enter', [
                style({opacity: 0, transform: 'translateX(+40px)'}),
                animate('300ms', style({opacity: 1, transform: 'translateX(0)'})),
            ])
        ])
    ]
})
export class LanguagePage implements OnInit {

    constructor(private wordService: WordService,
                private languageService: LanguageService,
                private route: ActivatedRoute,
                private toastController: ToastController) {
    }

    words: Word[];
    language: Language;
    id: string;
    addWordOption: boolean = false;
    originalWord: string;
    translatedWord: string;
    wordRequest: WordRequest;
    reverseMode: boolean = false;
    searchWord: string;

    doRefresh(event) {
        this.languageService.setLanguage();
        this.languageService.getAllLanguages().subscribe(()=>{
            event.target.complete();
        })

    }

    ngOnInit() {
        this.id = this.route.snapshot.paramMap.get('id');
        this.languageService.getLanguageWithId(this.id).subscribe(l => this.language = l);
    }

    searchOriginal(event) {
        this.searchWord = event.target.value;
        if (event?.target?.value?.trim()) {
            this.wordService.getAllWordsByOriginalWord(event.target.value.toLowerCase(), this.id).subscribe(words => this.words = words);
        } else {
            this.words = [];
        }
    }

    searchTranslated(event) {
        this.searchWord = event.target.value;
        if (event?.target?.value?.trim()) {
            this.wordService.getAllWordsByTranslatedWord(event.target.value.toLowerCase(), this.id).subscribe(words => this.words = words);
        } else {
            this.words = [];
        }
    }

    updateOriginalWord(event) {
        this.originalWord = event.target.value;
    }

    updateTranslateWord(event) {
        this.translatedWord = event.target.value;
    }

    clickSuccess() {
        if (this.originalWord?.trim() && this.translatedWord?.trim()) {
            this.addWordOption = false;
            this.wordRequest = new class implements WordRequest {
                langId: number;
                original: string;
                translated: string;
            };
            this.wordRequest.langId = Number(this.id);
            if (!this.reverseMode) {
                this.wordRequest.original = this.originalWord.toLowerCase();
                this.wordRequest.translated = this.translatedWord.toLowerCase();
            } else {
                this.wordRequest.translated = this.originalWord.toLowerCase();
                this.wordRequest.original = this.translatedWord.toLowerCase();
            }
            this.wordService.addNewWord(this.wordRequest).subscribe(() => {
                this.translatedWord = '';
                this.originalWord = '';
                this.wordRequest = null;
                this.refreshSearch();
                this.createWordToast();
            });
        }
    }

    clickX() {
        this.translatedWord = '';
        this.originalWord = '';
        this.addWordOption = false;
    }

    refreshSearch() {
        let event = {
            'target': {
                'value': this.searchWord
            }
        };
        if (this.reverseMode) {
            this.searchTranslated(event);
        } else {
            this.searchOriginal(event);
        }
    }

    reverseModeOn() {
        this.reverseMode = !this.reverseMode;
        this.refreshSearch();
    }

    deleteWord(id: number, word: string) {
        this.wordService.deleteWord(id).subscribe(
            () => {
                this.refreshSearch();
                this.deleteToast(word);
            }
        );
    }

    async deleteToast(word: string) {
        const delToast = await this.toastController.create({
            message: `${word} has been deleted`,
            duration: 2000
        });
        await delToast.present();
    }

    async createWordToast() {
        const createToast = await this.toastController.create({
            message: `Successfully added a word`,
            duration: 2000
        });
        await createToast.present();
    }

}
