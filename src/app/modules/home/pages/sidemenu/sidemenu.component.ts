import {Component, OnInit} from '@angular/core';
import {Language} from '../../interface/language';
import {ActivatedRoute} from '@angular/router';
import {animate, style, transition, trigger} from '@angular/animations';
import {WordService} from '../../word.service';
import {LanguageService} from '../../language.service';
import {LanguageRequest} from '../../interface/languageRequest';
import {LoadingController} from '@ionic/angular';
import {Observable} from 'rxjs';


@Component({
    selector: 'app-sidemenu',
    templateUrl: './sidemenu.component.html',
    styleUrls: ['./sidemenu.component.scss'],
    animations: [
        trigger('fadeSlideInOut', [
            transition(':enter', [
                style({opacity: 0, transform: 'translateX(-30px)'}),
                animate('300ms', style({opacity: 1, transform: 'translateX(0)'})),
            ])
        ]),
        trigger('fadeOutIn', [
            transition(':enter', [
                style({opacity: 0}),
                animate('100ms', style({opacity: 1})),
            ]),
            transition(':leave', [
                animate('100ms', style({opacity: 0})),
            ]),
        ])
    ]
})
export class SidemenuComponent implements OnInit {

    languages: Observable<Language[]>;

    constructor(private route: ActivatedRoute,
                private wordService: WordService,
                public languageService: LanguageService,
                private loadingController: LoadingController) {
    }

    inLang: boolean;
    nextLoc: string;
    addLanguageMode: boolean = false;
    fromLanguage: string;
    toLanguage: string;
    languageRequest: LanguageRequest;



    ngOnInit() {
        this.apiLoading();
        this.inLang = !!this.route.snapshot.paramMap.get('id');
        if (this.inLang) {
            this.nextLoc = '../';
        } else {
            this.nextLoc = '';
        }
    }




    async apiLoading() {
        const loading = await this.loadingController.create({
            message: 'Wait for the api to wake up'
        });
        await loading.present().then(() => {
            this.languageService.setLanguage();
          this.languages=this.languageService.languages;
          this.languages.subscribe(data=>{
              if (data.length>0){
                  loading.dismiss();
              }
          })
        });
    }

    updateToLanguage(event) {
        this.toLanguage = event.target.value;
    }

    updateFromLanguage(event) {
        this.fromLanguage = event.target.value;
    }

    addLanguage() {
        this.addLanguageMode = true;
    }

    successAdd() {
        if (this.fromLanguage?.length > 0 && this.toLanguage?.length > 0) {
            this.addLanguageMode = false;
            this.languageRequest = new class implements LanguageRequest {
                originalLanguage: string;
                translatedLanguage: string;
            };
            this.languageRequest.originalLanguage = this.fromLanguage;
            this.languageRequest.translatedLanguage = this.toLanguage;
            this.languageService.addNewLanguage(this.languageRequest).subscribe(
                () => {
                    this.languageRequest = null;
                    this.fromLanguage = '';
                    this.toLanguage = '';
                    this.languageService.setLanguage();
                  this.languages= this.languageService.languages;
                }
            );
        }
    }



    cancelAdd() {
        this.addLanguageMode = false;
        this.languageRequest = null;
        this.fromLanguage = '';
        this.toLanguage = '';
    }

}
