import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Constants} from '../constants/Constants';
import {Word} from './interface/word';
import {WordRequest} from './interface/wordRequest';


@Injectable({
    providedIn: 'root'
})
export class WordService {

    constructor(private _http: HttpClient) {
    }


    getAllWordsByOriginalWord(word: string, langId: string): Observable<Word[]> {
        return this._http.get<Word[]>(`${Constants.apiUrl}word/searchOriginal/${langId}/${word}`);
    }

    getAllWordsByTranslatedWord(word: string, langId: string): Observable<Word[]> {
        return this._http.get<Word[]>(`${Constants.apiUrl}word/searchTranslated/${langId}/${word}`);
    }

    addNewWord(wordRequest: WordRequest) {
        return this._http.post(`${Constants.apiUrl}word/add`, wordRequest);
    }

    deleteWord(id:number){
        return this._http.delete(`${Constants.apiUrl}word/delete/${id}`)
    }


}
